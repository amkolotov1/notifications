from drf_spectacular.utils import extend_schema
from rest_framework import viewsets

from api.v1.clients.serializers import ClientSerializer
from apps.clients.models import Client


@extend_schema(tags=["Клиенты"])
class ClientViewSet(viewsets.ModelViewSet):
    """Вьюсет клиентов"""

    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    http_method_names = ['get', 'post', 'patch', 'delete']

    @extend_schema(
        operation_id="auth_token_refresh", summary="Создать клиента",
        description="""
        Добавить нового клиента
        Пример:
        {
          "phone": "+79120000000",
          "tag": "my_tag",
          "timeZone": "Asia/Yekaterinburg"
        }
        """,
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Получить список клиентов",
        description="Получить постраничный список клиентов",
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Получить клиента",
        description="Получить информацию о клиенте по его ID",
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Частично обновить клиента",
        description="Обновить частично информацию о клиенте по его ID",
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Удалить клиента",
        description="Удалить клиента по его ID",
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
