from rest_framework.routers import DefaultRouter

from api.v1.clients.views import ClientViewSet

router = DefaultRouter()
router.register(r'', ClientViewSet, basename='clients')

urlpatterns = router.urls
