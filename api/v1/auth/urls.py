from django.urls import path

from api.v1.auth import views
from api.v1.auth.views import exchange_token

urlpatterns = [
    path('token/', views.TokenObtainCustomView.as_view()),
    path('token/refresh', views.TokenRefreshCustomView.as_view()),
    path('token/verify', views.TokenVerifyCustomView.as_view()),

    path('social/<backend>/', exchange_token),

]
