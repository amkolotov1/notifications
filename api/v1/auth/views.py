from drf_spectacular.utils import extend_schema
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework_simplejwt.views import \
    TokenObtainPairView, TokenRefreshView, TokenVerifyView
from social_django.utils import psa
from requests.exceptions import HTTPError

from api.v1.auth.serializers import SocialSerializer


@extend_schema(tags=["Авторизация"])
class TokenObtainCustomView(TokenObtainPairView):
    """Получить токены доступа по логину и паролю"""

    @extend_schema(
        operation_id="auth_token_obtain",
        summary="Получить токены",
    )
    def post(self, *args, **kwargs) -> Response:
        return super().post(*args, **kwargs)


@extend_schema(tags=["Авторизация"])
class TokenRefreshCustomView(TokenRefreshView):
    """Обновить токены доступа по выданному ранее refresh токену"""

    @extend_schema(
        operation_id="auth_token_refresh",
        summary="Обменять refresh токен на новые токены",
    )
    def post(self, *args, **kwargs) -> Response:
        return super().post(*args, **kwargs)


@extend_schema(tags=["Авторизация"])
class TokenVerifyCustomView(TokenVerifyView):
    """Проверить токен доступа"""

    @extend_schema(
        operation_id="auth_token_verify",
        summary="Проверить токен",
    )
    def post(self, *args, **kwargs) -> Response:
        return super().post(*args, **kwargs)


@extend_schema(
    tags=["Авторизация"], operation_id="auth_token_refresh", auth=[],
    summary="Получение JWT токенов после OAuth авторизации",
    request={
      "application/json": {
          "description": "Получить JWT токен",
          "type": "object",
          "properties": {
              "access_token": {
                  "type": "string",
                  "minLength": 1
              },
          },
          "required": [
              "access_token",
          ]
      }
    }
)
@api_view(http_method_names=['POST'])
@permission_classes([AllowAny])
@psa()
def exchange_token(request, backend):
    """
    Параметр backend = 'github'.
    Для получения токена Github выйти из учетной записи администратора,
    перейти по адресу http://localhost:8000/oauth, нажать на кнопку,
    после успешной авторизации зайти в панель администратора под суперюзером
    в раздел 'User social auths',
    выбрать пользователя и из поля 'Extra data' скопировать 'access_token',
    отправить запрос и получить JWT токены приложения.
    """
    serializer = SocialSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        try:
            user = request.backend.do_auth(serializer.validated_data['access_token'])
        except HTTPError as e:
            return Response(
                {'errors': {
                    'token': 'Токен не валидный',
                    'detail': str(e),
                }},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if user:
            if user.is_active:
                token = RefreshToken.for_user(user)
                return Response(
                    {'access': str(token.access_token), 'refresh': str(token)}
                )
            else:
                return Response(
                    {'errors': {'non_field': 'Пользователь не активен'}},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        else:
            return Response(
                {'errors': {'non_field': 'Не пройдена авторизация'}},
                status=status.HTTP_400_BAD_REQUEST,
            )
