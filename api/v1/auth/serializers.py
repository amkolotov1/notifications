from rest_framework import serializers


class SocialSerializer(serializers.Serializer):
    """Сериализатор для получения JWT токена приложения"""
    access_token = serializers.CharField(
        allow_blank=False,
        trim_whitespace=True,
    )
