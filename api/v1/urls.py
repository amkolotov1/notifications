from django.urls import path, include
from drf_spectacular.views import \
    SpectacularAPIView, SpectacularSwaggerView, SpectacularRedocView

urlpatterns = [
    path('schema/', SpectacularAPIView.as_view(), name="schema-v1"),
    path('docs/', SpectacularSwaggerView.as_view(url_name="schema-v1")),
    path("redoc/", SpectacularRedocView.as_view(url_name="schema-v1")),

    path('auth/', include('api.v1.auth.urls')),
    path('client/', include('api.v1.clients.urls')),
    path('mailing/', include('api.v1.mailings.urls')),
]

handler400 = 'rest_framework.exceptions.bad_request'
handler500 = 'rest_framework.exceptions.server_error'
