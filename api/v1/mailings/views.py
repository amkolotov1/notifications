from django.db.models import OuterRef, Func, F, Subquery
from drf_spectacular.utils import extend_schema
from rest_framework import viewsets

from api.v1.mailings.serializers import MailingSerializer, RetrieveMailingSerializer
from apps.mailings.models import Mailing


@extend_schema(tags=["Рассылки"])
class MailingViewSet(viewsets.ModelViewSet):
    """Вьюсет рассылок"""

    queryset = Mailing.objects.annotate(
            messages=Subquery(Mailing.get_messages_subquery().get('subquery')),
            messages_success=Subquery(Mailing.get_messages_subquery().get('subquery_success')),
            messages_error=Subquery(Mailing.get_messages_subquery().get('subquery_error')),
            messages_overdue=Subquery(Mailing.get_messages_subquery().get('subquery_overdue')),
        )
    serializer_classes = {
        'list': RetrieveMailingSerializer,
        'retrieve': RetrieveMailingSerializer,
    }
    default_serializer_class = MailingSerializer
    http_method_names = ['get', 'post', 'patch', 'delete']

    def get_serializer_class(self):
        return self.serializer_classes.get(self.action, self.default_serializer_class)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Создать рассылку",
        description="""
        Добавить новую рассылку
        Пример:
        {
          "text": "my text",
          "code": "912",
          "tag": "my_tag",
          "startAt": "2023-06-27T13:30:54Z",
          "finishAt": "2023-06-27T13:30:54Z",
          "localInterval": "00:00-01:30"
        }
        """,
    )
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Получить список рассылок",
        description="Получить постраничный список рассылок",
    )
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Получить рассылку",
        description="Получить информацию о рассылке по ее ID",
    )
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Частично обновить рассылку",
        description="Обновить частично информацию о рассылке по ее ID",
    )
    def partial_update(self, request, *args, **kwargs):
        return super().partial_update(request, *args, **kwargs)

    @extend_schema(
        operation_id="auth_token_refresh", summary="Удалить рассылку",
        description="Удалить рассылку",
    )
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)
