from rest_framework import serializers

from apps.mailings.models import Mailing


class RetrieveMailingSerializer(serializers.ModelSerializer):
    """Сериализатор рассылки"""

    messages = serializers.IntegerField(required=False)
    messages_success = serializers.IntegerField(required=False)
    messages_error = serializers.IntegerField(required=False)
    messages_overdue = serializers.IntegerField(required=False)

    class Meta:
        model = Mailing
        fields = [
            'id', 'text', 'code', 'tag', 'start_at', 'finish_at', 'is_processed',
            'created_at', 'updated_at', 'messages_success', 'messages_error',
            'messages', 'messages_overdue',
        ]
        read_only_fields = [
            'id', 'created_at', 'updated_at', 'messages_success',
            'messages_error', 'messages', 'messages_overdue', 'is_processed'
        ]


class MailingSerializer(serializers.ModelSerializer):
    """Сериализатор рассылки"""

    class Meta:
        model = Mailing
        fields = [
            'id', 'text', 'code', 'tag', 'start_at',
            'finish_at', 'local_interval'
        ]
        read_only_fields = ['id', 'created_at', 'updated_at']

