from rest_framework.routers import DefaultRouter

from api.v1.mailings.views import MailingViewSet

router = DefaultRouter()
router.register(r'', MailingViewSet, basename='mailings')

urlpatterns = router.urls
