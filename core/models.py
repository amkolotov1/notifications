from django.db import models


class BaseModel(models.Model):
    """Базовая модель"""
    created_at = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True
    )
    updated_at = models.DateTimeField(
        'Дата обновления', auto_now=True, db_index=True
    )

    class Meta:
        ordering = ('-updated_at',)
        abstract = True
