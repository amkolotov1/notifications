from django.shortcuts import render


def oauth(request):
    """Возвращает страницу с кнопкой OAuth авторизации"""
    return render(request, 'oauth.html')
