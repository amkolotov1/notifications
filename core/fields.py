from django.db.models import fields

from core.validators import validate_phone


class PhoneField(fields.CharField):
    """Поле номера телефона"""

    default_validators = [validate_phone]
