import requests

from core.exeptions import UnexpectedResponseCustomException


class Sender:
    """Клиент для отправки сообщений"""

    def __init__(self,  url: str, token: str):
        self._url = url
        self._token = token

    def _make_request(self, method: str, postfix: str, data: dict) -> dict:
        """Выполняет запрос к внешнему сервису"""

        kwargs = {'headers': {'authorization': f'Bearer {self._token}'}}

        if method == 'get':
            kwargs['params'] = data
        else:
            kwargs['json'] = data

        response = requests.request(method, f'{self._url}{postfix}', **kwargs)

        if not str(response.status_code).startswith('2'):
            raise UnexpectedResponseCustomException(response)

        return response.json()

    def send_message(self, message_id: int, data: dict) -> dict:
        """Отправляет сообщение"""
        return self._make_request('post', f'send/{message_id}', data)