from datetime import datetime

import phonenumbers
from django.core.exceptions import ValidationError


def validate_phone(phone: str) -> str:
    """Провалидировать номер телефона"""

    try:
        phone = phonenumbers.parse(phone)
    except phonenumbers.NumberParseException:
        raise ValidationError("Неверный формат номера телефона")

    if not phonenumbers.is_valid_number(phone):
        raise ValidationError("Неверный формат номера телефона")

    if str(phone.country_code) not in ('7',):
        raise ValidationError("Номер из данной страны не поддерживается")

    return f"+{phone.country_code}{phone.national_number}"


def validate_interval(interval: str) -> str:
    """Провалидировать временной интервал"""
    try:
        from_str, to_str = interval.split('-')
        time_from = datetime.strptime(from_str, "%H:%M").time()
        time_to = datetime.strptime(to_str, "%H:%M").time()
        if time_from >= time_to:
            raise

    except Exception:
        raise ValidationError("Некорректно указан временной интервал")

    else:
        return interval


