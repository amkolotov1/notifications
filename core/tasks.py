import celery

class BaseTaskWithRetry(celery.Task):
    """Базовая селери таска с ретраем"""

    autoretry_for = (Exception,)
    retry_kwargs = {'max_retries': 5}
    retry_backoff = True
    retry_jitter = True
