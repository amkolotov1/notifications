from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from apps.mailings.models import Mailing

text = 'test text'
code = '912'
tag = 'first_tag'


class MailingApiTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_username')
        Mailing.objects.create(
            text=text, code=code, tag=tag,
            start_at='2023-06-26T02:20:56.893Z',
            finish_at='2023-06-26T02:20:56.893Z'
        )
        self.token = RefreshToken.for_user(self.user).access_token

    def test_get(self):
        mailing = Mailing.objects.first()
        url = reverse('mailings-detail', kwargs={'pk': mailing.id})

        response = self.client.get(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(text, response.data.get('text'))
        self.assertEqual(code, response.data.get('code'))
        self.assertEqual(tag, response.data.get('tag'))

    def test_list(self):
        url = reverse('mailings-list')

        response = self.client.get(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIn('results', response.data)
        self.assertEqual(1, response.data.get('count'))
        self.assertIsNone(response.data.get('next'))

    def test_create(self):
        url = reverse('mailings-list')
        data = {
            "text": "second text",
            "code": "922",
            "tag": "second_tag",
            "start_at": "2023-06-26T02:20:56.893Z",
            "finish_at": "2023-06-26T02:20:56.893Z"
        }

        self.assertEqual(1, Mailing.objects.count())

        response = self.client.post(
            url, data=data, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(data.get('text'), response.data.get('text'))
        self.assertEqual(data.get('code'), response.data.get('code'))
        self.assertEqual(data.get('tag'), response.data.get('tag'))

        self.assertEqual(2, Mailing.objects.count())

    def test_update(self):
        mailing = Mailing.objects.first()
        url = reverse('mailings-detail', kwargs={'pk': mailing.id})

        self.assertEqual('test text', mailing.text)

        data = {
            "text": "updated test text",
        }
        response = self.client.patch(
            url, data=data, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        mailing.refresh_from_db()
        self.assertEqual('updated test text', mailing.text)

    def test_delete(self):
        mailing = Mailing.objects.first()
        url = reverse('mailings-detail', kwargs={'pk': mailing.id})

        self.assertEqual(1, Mailing.objects.count())

        response = self.client.delete(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(0, Mailing.objects.count())
