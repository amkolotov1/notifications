import json
from datetime import timedelta
from unittest import mock

from django.test import TestCase

from django.conf import settings
from django.utils import timezone
from requests import Response

from config.celery import app
from apps.clients.models import Client
from apps.mailings.models import Message, Mailing
from apps.mailings import tasks


text = 'test text'
code = '912'
tag = 'first_tag'
phone = '+79120000001'
time_zone = 'Asia/Yekaterinburg'


def mocked_request(*args, **kwargs):
    response_content = {'code': 0, 'message': kwargs['json']['text']}
    response = Response()
    response.status_code = 200
    response._content = str.encode(json.dumps(response_content))
    return response


def mocked_request_error(*args, **kwargs):
    response_content = {'code': -1, 'message': kwargs['json']['text']}
    response = Response()
    response.status_code = 400
    response._content = str.encode(json.dumps(response_content))
    return response


class MailingTasksTestCase(TestCase):
    def setUp(self):
        app.conf.update(CELERY_ALWAYS_EAGER=True)
        self.mailing = Mailing.objects.create(
            text=text, code=code, tag=tag,
            start_at=timezone.now() - timedelta(minutes=1),
            finish_at=timezone.now() + timedelta(minutes=1)
        )
        self.client = Client.objects.create(
            phone=phone, tag=tag, time_zone=time_zone
        )
        self.message = Message.objects.create(
            mailing=self.mailing, client=self.client
        )
        self.url = settings.MAILING_URL
        self.token = settings.MAILING_TOKEN

    def test_send_message_success(self):
        with mock.patch(
                'requests.request', side_effect=mocked_request
        ) as mock_req:
            message_id = self.message.id
            self.assertEqual(Message.Statuses.WAIT, self.message.status,)
            tasks.send_message(message_id)
            self.message.refresh_from_db()
            self.assertTrue(mock_req.assert_called_once)
            self.assertEqual(Message.Statuses.SUCCESS, self.message.status)

    def test_send_message_error(self):
        with mock.patch(
                'requests.request', side_effect=mocked_request_error
        ) as mock_req:
            message_id = self.message.id
            self.assertEqual(Message.Statuses.WAIT, self.message.status,)
            tasks.send_message(message_id)
            self.message.refresh_from_db()
            self.assertTrue(mock_req.assert_called_once)
            self.assertEqual(Message.Statuses.ERROR, self.message.status)

    def test_send_message_overdue(self):
        self.mailing.finish_at = timezone.now() - timedelta(minutes=1)
        self.mailing.save()
        with mock.patch('requests.request', side_effect=mocked_request) as mock_req:
            message_id = self.message.id
            self.assertEqual(Message.Statuses.WAIT, self.message.status,)
            tasks.send_message(message_id)
            self.message.refresh_from_db()
            self.assertTrue(mock_req.assert_called_once)
            self.assertEqual(Message.Statuses.OVERDUE, self.message.status)

