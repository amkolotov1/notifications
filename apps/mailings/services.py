from datetime import datetime

from django.utils import timezone
from pytz import timezone as tz

from apps.mailings.models import Message


def check_message_time(message: Message):
    """Проверка времени запуска таски"""
    try:
        mailing = message.mailing
        client = message.client
        if mailing.start_at <= timezone.now() < mailing.finish_at:
            local_now = datetime.now(tz(client.time_zone)).time()
            local_from_str, local_to_str = mailing.local_interval.split('-')
            local_from = datetime.strptime(local_from_str, "%H:%M").time()
            local_to = datetime.strptime(local_to_str, "%H:%M").time()

            return local_from <= local_now <= local_to

    except Exception:
        pass


def get_formatted_phone(phone_str: str) -> int:
    """Возвращает номер телефона для отправки на внешний сервис"""
    return int(phone_str.replace('+', ''))
