from django.db import models
from django.db.models import OuterRef, Func, F

from core.models import BaseModel
from core.validators import validate_interval


class Mailing(BaseModel):
    """Модель рассылки"""

    start_at = models.DateTimeField('Время начала рассылки')
    finish_at = models.DateTimeField('Время окончания рассылки')
    text = models.CharField('Текст сообщения', max_length=255)
    code = models.CharField('Код оператора', max_length=3)
    tag = models.CharField('Тег', max_length=32)
    is_processed = models.BooleanField('Обработана', default=False)
    local_interval = models.CharField(
        'Временной интервал', max_length=11, default='00:00-23:59',
        validators=[validate_interval]
    )

    class Meta:
        verbose_name = "Рассылка"
        verbose_name_plural = "Рассылки"

    def __str__(self):
        return f'{self.pk} - {self.start_at}'

    @staticmethod
    def get_messages_subquery():
        return {
            'subquery': Message.objects.filter(
                mailing_id=OuterRef('pk')
            ).annotate(
                count=Func(F('id'), function='Count')
            ).values('count'),

            'subquery_success': Message.objects.filter(
                status=Message.Statuses.SUCCESS, mailing_id=OuterRef('pk')
            ).annotate(
                count=Func(F('id'), function='Count')
            ).values('count'),

            'subquery_error': Message.objects.filter(
                status=Message.Statuses.ERROR, mailing_id=OuterRef('pk')
            ).annotate(
                count=Func(F('id'), function='Count')
            ).values('count'),

            'subquery_overdue': Message.objects.filter(
                status=Message.Statuses.OVERDUE, mailing_id=OuterRef('pk')
            ).annotate(
                count=Func(F('id'), function='Count')
            ).values('count')
        }


class Message(BaseModel):
    """Модель сообщения"""

    class Statuses(models.TextChoices):
        WAIT = 'WAIT', 'Ожидание'
        SUCCESS = 'SUCCESS', 'Успешно'
        ERROR = 'ERROR', 'Ошибка'
        OVERDUE = 'OVERDUE', 'Просрочено'

    status = models.CharField(
        'Статус отправки', choices=Statuses.choices,
        max_length=7, default=Statuses.WAIT
    )
    send_at = models.DateTimeField('Дата отправки', null=True, blank=True)
    mailing = models.ForeignKey(
        Mailing, on_delete=models.CASCADE,
        verbose_name='Рассылка', related_name='mailing_messages'
    )
    client = models.ForeignKey(
        'clients.Client', on_delete=models.CASCADE,
        verbose_name='Клиент', related_name='client_messages'
    )

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"

    def __str__(self):
        return f'{self.mailing} - {self.client}'
