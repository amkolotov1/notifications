from datetime import timedelta

from celery import shared_task
from celery.app import task
from celery.utils.log import get_task_logger
from django.core.mail import send_mail
from django.db.models import Subquery
from django.utils import timezone

from apps.clients.models import Client
from apps.mailings.models import Mailing, Message
from apps.mailings.services import check_message_time, get_formatted_phone
from config import settings
from config.celery import app
from core.services.sender import Sender
from core.tasks import BaseTaskWithRetry


logger = get_task_logger(__name__)


@shared_task
def check_mailings():
    """Запускает активные рассылки"""

    logger.info(f'check_mailings start')

    try:
        mailings = Mailing.objects.filter(
            start_at__lte=timezone.now(), finish_at__gt=timezone.now(),
            is_processed=False,
        )
        for mailing in mailings:
            app.send_task(
                'apps.mailings.tasks.create_messages',
                args=[mailing.id], queue='default'
            )
            mailing.is_processed = True
            mailing.save(update_fields=['is_processed'])

        logger.info(f'check_mailings finish')

    except Exception as e:
        logger.error(f'check_mailings error: {e}')


@shared_task
def create_messages(mailing_id: int):
    """Создает сообщения для рассылки"""

    logger.info(f'create_messages mailing id {mailing_id} start')

    try:
        if mailing := Mailing.objects.filter(pk=mailing_id).first():
            clients_filtered = Client.objects.filter(
                code=mailing.code, tag=mailing.tag
            )
            for client in clients_filtered:
                message, created = Message.objects.get_or_create(
                    mailing=mailing, client=client
                )
                if created:
                    app.send_task(
                        'apps.mailings.tasks.send_message',
                        args=[message.id], queue='sender'
                    )

        logger.info(f'create_messages mailing id {mailing_id} finish')

    except Exception as e:
        logger.error(f'create_messages mailing id {mailing_id} error: {e}')


@shared_task(bind=True, base=BaseTaskWithRetry)
def send_message(self: task, message_id: int):
    """Отправляет сообщение"""

    logger.info(f'send_message start: {message_id}')

    if message := Message.objects.filter(pk=message_id).first():

        try:
            if message.status != message.Statuses.WAIT:
                return

            if check_message_time(message):
                sender = Sender(settings.MAILING_URL, settings.MAILING_TOKEN)
                data = {
                    'id': message.id,
                    'phone': get_formatted_phone(message.client.phone),
                    'text': message.mailing.text
                }
                response_data = sender.send_message(message.id, data)
                if response_data.get('code') == 0:
                    message.status = Message.Statuses.SUCCESS
                else:
                    message.status = Message.Statuses.ERROR

            else:
                message.status = Message.Statuses.OVERDUE

            logger.info(
                f'send_message finish: id - {message_id},'
                f' client_id: {message.client.id},'
                f' mailing_id: {message.mailing.id}'
            )

        except Exception as e:
            message.status = Message.Statuses.ERROR
            logger.error(
                f'send_message {message_id} error: {e}'
                f'client_id: {message.client.id},'
                f' mailing_id: {message.mailing.id}'
            )

        finally:
            message.send_at = timezone.now()
            message.save(update_fields=['status', 'send_at'])


@shared_task
def send_mailings_statistics():
    """Отправляет статистику по рассылкам за сутки"""

    logger.info(f'send_mailings_statistics start')

    try:
        mailings = Mailing.objects.filter(
            updated_at__gte=timezone.now() - timedelta(hours=24),
            is_processed=True,
        ).annotate(
            messages=Subquery(Mailing.get_messages_subquery().get('subquery')),
            messages_success=Subquery(
                Mailing.get_messages_subquery().get('subquery_success')
            ),
            messages_error=Subquery(
                Mailing.get_messages_subquery().get('subquery_error')
            ),
            messages_overdue=Subquery(
                Mailing.get_messages_subquery().get('subquery_overdue')
            ),
        )
        if mailings:
            send_mail(
                'Статистика рассылок за сутки',
                str(list(mailings.values(
                    'id', 'text', 'code', 'tag', 'messages', 'messages_success',
                    'messages_error', 'messages_overdue'
                ))),
                settings.DEFAULT_FROM_EMAIL,
                [settings.STATISTIC_EMAIL],
                fail_silently=False,
            )
        logger.info(f'send_mailings_statistics finish')

    except Exception as e:
        logger.error(f'send_mailings_statistics error: {e}')
