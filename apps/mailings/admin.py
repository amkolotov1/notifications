from django.contrib import admin
from django.db.models import Subquery

from apps.mailings.models import Mailing, Message


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'code', 'tag', 'is_processed', 'messages',
        'messages_success', 'messages_error', 'messages_overdue'
    ]
    list_filter = ['code', 'tag', 'is_processed']
    ordering = ['-created_at']
    search_fields = ['id', 'text']

    def get_queryset(self, request):
        queryset = super().get_queryset(request).annotate(
            messages=Subquery(Mailing.get_messages_subquery().get('subquery')),
            messages_success=Subquery(
                Mailing.get_messages_subquery().get('subquery_success')
            ),
            messages_error=Subquery(
                Mailing.get_messages_subquery().get('subquery_error')
            ),
            messages_overdue=Subquery(
                Mailing.get_messages_subquery().get('subquery_overdue')
            ),
        )
        return queryset

    def messages(self, obj):
        return obj.messages

    def messages_success(self, obj):
        return obj.messages_success

    def messages_error(self, obj):
        return obj.messages_error

    def messages_overdue(self, obj):
        return obj.messages_overdue

    messages.short_description = 'Сообщений всего'
    messages_success.short_description = 'Успешно отправлены'
    messages_error.short_description = 'Ошибка при отправке'
    messages_overdue.short_description = 'Просрочены'


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = [
        'id', 'mailing', 'client', 'status', 'send_at',
    ]
    list_filter = ['mailing', 'client', 'status']
    ordering = ['-created_at']
    search_fields = ['id']


