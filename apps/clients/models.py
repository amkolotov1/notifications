import phonenumbers
import pytz
from django.conf import settings
from django.db import models

from core.fields import PhoneField
from core.models import BaseModel


class Client(BaseModel):
    """Модель клиента"""

    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone = PhoneField('Телефон', max_length=20, unique=True)
    code = models.CharField('Код оператора', max_length=3)
    tag = models.CharField('Тег', max_length=32)
    time_zone = models.CharField(
        max_length=100, choices=TIMEZONES, default=settings.TIME_ZONE
    )

    class Meta:
        verbose_name = "Клиент"
        verbose_name_plural = "Клиенты"

    def __str__(self):
        return f'{self.pk}-{self.phone}'

    def save(self, *args, **kwargs):
        self.code = str(phonenumbers.parse(self.phone).national_number)[:3]
        super().save(*args, **kwargs)
