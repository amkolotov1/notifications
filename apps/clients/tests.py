from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from apps.clients.models import Client

phone = '+79120000001'
tag = 'first_tag'
time_zone = 'Asia/Yekaterinburg'


class ClientApiTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(username='test_username')
        Client.objects.create(
            phone=phone, tag=tag, time_zone=time_zone
        )
        self.token = RefreshToken.for_user(self.user).access_token

    def test_get(self):
        client = Client.objects.first()
        url = reverse('clients-detail', kwargs={'pk': client.id})

        response = self.client.get(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertEqual(phone, response.data.get('phone'))
        self.assertEqual(tag, response.data.get('tag'))
        self.assertEqual(time_zone, response.data.get('time_zone'))

    def test_list(self):
        url = reverse('clients-list')

        response = self.client.get(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        self.assertIn('results', response.data)
        self.assertEqual(1, response.data.get('count'))
        self.assertIsNone(response.data.get('next'))

    def test_create(self):
        url = reverse('clients-list')
        data = {
            "phone": "+79120000002",
            "tag": "second_tag",
            "timeZone": "Asia/Yekaterinburg"
        }

        self.assertEqual(1, Client.objects.count())

        response = self.client.post(
            url, data=data, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assertEqual(data.get('phone'), response.data.get('phone'))
        self.assertEqual(data.get('tag'), response.data.get('tag'))
        self.assertEqual(data.get('timeZone'), response.data.get('time_zone'))

        self.assertEqual(2, Client.objects.count())

    def test_update(self):
        client = Client.objects.first()
        url = reverse('clients-detail', kwargs={'pk': client.id})

        self.assertEqual('first_tag', client.tag)

        data = {
            "tag": "second_tag",
        }
        response = self.client.patch(
            url, data=data, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_200_OK, response.status_code)
        client.refresh_from_db()
        self.assertEqual('second_tag', client.tag)

    def test_delete(self):
        client = Client.objects.first()
        url = reverse('clients-detail', kwargs={'pk': client.id})

        self.assertEqual(1, Client.objects.count())

        response = self.client.delete(
            url, format='json',
            headers={'Authorization': f'Bearer {self.token}'}
        )

        self.assertEqual(status.HTTP_204_NO_CONTENT, response.status_code)
        self.assertEqual(0, Client.objects.count())
