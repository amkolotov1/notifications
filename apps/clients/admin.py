from django.contrib import admin

from apps.clients.models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['id', 'phone', 'code', 'tag', 'time_zone']
    list_filter = ['code', 'tag']
    ordering = ['-created_at']
    search_fields = ['id', 'phone', 'time_zone']

