def only_api_endpoints(endpoints: list[tuple]) -> list[tuple]:
    """Отфильтровать только эндпоинты, связанные с api"""

    api_endpoints = []

    for path, *rest in endpoints:
        if path.startswith("/api/"):
            api_endpoints.append((path, *rest))

    return api_endpoints
