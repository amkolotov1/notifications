from django.contrib import admin
from django.urls import path, include

from core.views import oauth

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('api.v1.urls')),
    path('oauth/', oauth),
    path('', include('social_django.urls', namespace='social')),
]
