import os

import celery.signals
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'config.settings')

app = Celery('proj')

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@celery.signals.setup_logging.connect
def on_celery_setup_logging(**kwargs):
    pass
